from flask import Flask
from flask_login import LoginManager
from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__, template_folder='templates')
app.secret_key = 'Aboba'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['UPLOAD_FOLDER'] = 'static/img'
app.config['MAIL_SERVER']='smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'ipzk211_yeai@student.ztu.edu.ua'
app.config['MAIL_PASSWORD'] = '966605'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}
mail = Mail(app)
db = SQLAlchemy(app)
manager = LoginManager(app, db)
manager.login_view = '/login'

