from cfg import app, db
import controllers.main_controller
import controllers.user_controller
import controllers.basket_controller
import controllers.deliver_controller
import controllers.feedback_controller

db.create_all()
if __name__ == "__main__":
    app.run(host='0.0.0.0', port='80')

