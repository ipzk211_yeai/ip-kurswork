create table product_item
(
	id INTEGER not null
		primary key,
	pic VARCHAR(50) not null,
	title VARCHAR(50) not null,
	description VARCHAR(200) not null,
	price FLOAT not null,
	category VARCHAR(50) not null
);

