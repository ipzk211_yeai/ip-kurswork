create table feedback
(
	id INTEGER not null
		primary key,
	userMessage VARCHAR(250) not null,
	userEmail VARCHAR(64) not null,
	userLogin VARCHAR(40) not null
);

