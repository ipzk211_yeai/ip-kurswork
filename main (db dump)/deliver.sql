create table deliver
(
	id INTEGER not null
		primary key,
	products VARCHAR(100) not null,
	count VARCHAR(100) not null,
	deliverTo VARCHAR(100) not null,
	userEmail VARCHAR(64) not null,
	userLogin VARCHAR(40) not null,
	userFIO VARCHAR(60) not null,
	userNumber VARCHAR(10) not null
);

