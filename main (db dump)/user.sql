create table user
(
	id INTEGER not null
		primary key,
	login VARCHAR(24) not null
		unique,
	password VARCHAR(100) not null,
	email VARCHAR(64) not null,
	deliver VARCHAR(100),
	admin BOOLEAN,
	fio VARCHAR(60) not null,
	number VARCHAR(10)
);

