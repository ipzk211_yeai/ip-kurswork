import itertools

import flask_user
from flask import render_template, request, redirect, url_for, abort
from flask_login import login_required
from models.product_item import ProductItem
from models.deliver import Deliver
from cfg import app, manager, db, mail
from flask_mail import Mail,Message
import json
from models.user import User


@app.route('/deliver')
@login_required
def deliverInfo():
    if flask_user.current_user.admin:
        total = []
        delivers = Deliver.query.all()
        for el in delivers:
            names = []
            tmp = json.loads(el.products)
            for le in tmp:
                names.append(ProductItem.query.filter_by(id=le).first())
            total.append([el, names])
        return render_template('deliver-info.html', json=json, total=total, itertools=itertools)
    else:
        return abort(404)



@app.route('/deliver-accept/<int:id>')
@login_required
def deliverAccept(id):
    if flask_user.current_user.admin:
        deliver = Deliver.query.get_or_404(id)
        try:
            msg = Message('ArtiShop', sender=app.config['MAIL_USERNAME'], recipients=[deliver.userEmail])
            msg.body = "Спасибо за покупку, товар отправлен на ваш адрес"
            mail.send(msg)
            db.session.delete(deliver)
            db.session.commit()
            return redirect('/deliver')
        except:
            return abort(404)
    else:
        return abort(404)


@app.route('/deliver-delete/<int:id>', methods=['POST', 'GET'])
@login_required
def deliverDelete(id):
    if request.method == "POST":
        if flask_user.current_user.admin:
            deliver = Deliver.query.get_or_404(id)
            try:
                db.session.delete(deliver)
                db.session.commit()
                return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}
            except:
                return "При удалении произошла ошибка"
        else:
            return abort(404)
