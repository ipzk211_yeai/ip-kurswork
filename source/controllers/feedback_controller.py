import flask_user
from flask import render_template, request, redirect, url_for, abort, flash
from flask_login import login_required
from models.product_item import ProductItem
from models.deliver import Deliver
from cfg import app, manager, db, mail
from flask_mail import Mail, Message
import json
from models.user import User
from models.feedback import Feedback


@app.route('/leave-feedback', methods=['POST', 'GET'])
@login_required
def leaveFeedback():
    if request.method == 'POST':
        msg = request.form['msg']
        umail = flask_user.current_user.email
        ulogin = flask_user.current_user.login
        try:
            feedback = Feedback(userMessage=msg, userEmail=umail, userLogin=ulogin)
            db.session.add(feedback)
            db.session.commit()
            return render_template('feedback-finish.html')
        except:
            flash('Произошла ошибка, попробуйте позже.')
            return render_template('leave-feedback.html')
    else:
        return render_template('leave-feedback.html')


@app.route('/feedback')
@login_required
def feedbackInfo():
    if flask_user.current_user.admin:
        feedback = Feedback.query.all()
        return render_template('feedback.html', feedback=feedback)
    else:
        return abort(404)


@app.route('/feedback/answer/<int:id>', methods=['POST', 'GET'])
@login_required
def feedbackAnswer(id):
    if flask_user.current_user.admin:
        feedback = Feedback.query.filter_by(id=id).first()
        if request.method == "POST":
            mes = request.form['msg']
            msg = Message('ArtiShop', sender=app.config['MAIL_USERNAME'], recipients=[feedback.userEmail])
            msg.body = mes
            mail.send(msg)
            db.session.delete(feedback)
            db.session.commit()
            return redirect('/feedback')
        else:
            feedback = Feedback.query.filter_by(id=id).first()
            return render_template('feedback-answer.html', feedback=feedback)
    else:
        return abort(404)


@app.route('/feedback/delete/<int:id>')
@login_required
def feedbackDelete(id):
    if flask_user.current_user.admin:
        feedback = Feedback.query.filter_by(id=id).first()
        db.session.delete(feedback)
        db.session.commit()
        return redirect('/feedback')
    else:
        return abort(404)
