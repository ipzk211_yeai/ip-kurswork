import flask_user
from flask import render_template, request, redirect, url_for
from models.product_item import ProductItem
from models.deliver import Deliver
from cfg import app, manager, db, mail
from flask_mail import Mail,Message
import json
from models.user import User


@app.route('/shopping-cart', methods=['GET', 'POST'])
def cart():
    allcookies = request.cookies
    products = []
    count = []
    for el in allcookies:
        tmpcount = request.cookies.get(el)
        try:
            tmpproduct = ProductItem.query.filter_by(id=int(el)).first()
            products.append(tmpproduct)
            count.append(tmpcount)
        except:
            print('')
    sum = 0
    if products:
        k = 0
        for el in products:
            sum = sum + float(el.price * int(count[k]))
            k = k + 1

    return render_template('cart.html', products=products, count=count, sum=sum)


@app.route('/purchase', methods=['GET', 'POST'])
def purchase():
    if request.method == 'POST':
        user = User.query.filter_by(id=flask_user.current_user.id).first()
        user.deliver = request.form['deliver']
        db.session.commit()
        return redirect(url_for('purchase'))
    else:
        if flask_user.current_user.deliver != "Нет данных":
            allcookies = request.cookies
            productsCart = []
            countCart = []
            for el in allcookies:
                tmpcount = request.cookies.get(el)
                try:
                    tmpproduct = ProductItem.query.filter_by(id=int(el)).first()
                    productsCart.append(tmpproduct.id)
                    countCart.append(tmpcount)
                except:
                    print('')
            print(productsCart)
            print(countCart)
            newDeliver = Deliver(products=json.dumps(productsCart), count=json.dumps(countCart), deliverTo=flask_user.current_user.deliver,
                                 userEmail=flask_user.current_user.email, userLogin=flask_user.current_user.login, userFIO=flask_user.current_user.fio,
                                 userNumber=flask_user.current_user.number)
            db.session.add(newDeliver)
            db.session.commit()
            return render_template('success.html')
        else:
            return render_template('deliver.html')
