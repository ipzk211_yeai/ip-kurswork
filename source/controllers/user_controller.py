import flask_user
from flask import flash, request, redirect, render_template, url_for, abort
from cfg import manager, app, db
from models.user import User
from werkzeug.security import check_password_hash, generate_password_hash
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user
from models.LoginForm import LoginForm
from models.RegisterForm import RegisterForm


@app.route('/login', methods=['GET', 'POST'])
def login_page():
    form = LoginForm()
    if form.validate_on_submit():
        if form.login.data and form.password.data:
            user = User.query.filter_by(login=form.login.data).first()

            if user and check_password_hash(user.password, str(form.password.data)):
                login_user(user)

                next_page = request.args.get('next')
                if next_page:
                    return redirect(next_page)
                else:
                    return redirect('/0')
            else:
                flash('Логин или пароль не коректны')
                return render_template('login.html', form=form)
        else:
            flash('Поля должны быть заполнены')
            return render_template('login.html', form=form)
    else:
        return render_template('login.html', form=form)


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        try:
            if not (form.login.data or form.password2.data or form.password1.data):
                flash('Все поля должны быть заполнены')
                return render_template('register.html', form=form)
            elif form.password1.data != form.password2.data:
                flash('Пароли не сходятся')
                return render_template('register.html', form=form)
            else:
                hash_pwd = generate_password_hash(form.password1.data)
                if form.deliver.data:
                    new_user = User(login=form.login.data, password=hash_pwd, email=form.email.data,
                                    deliver=form.deliver.data,fio=form.fio.data,number=form.number.data, admin=False)
                else:
                    new_user = User(login=form.login.data, password=hash_pwd, email=form.email.data,
                                    deliver='Нет данных', fio=form.fio.data, number=form.number.data, admin=False)
                db.session.add(new_user)
                db.session.commit()

                return redirect(url_for('login_page', form=form))
        except:
            flash('Такой пользователь уже существует')
            return render_template('register.html', form=form)
    else:
        flash('Не правильный email')
        return render_template('register.html', form=form)


@app.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/about-me', methods=['GET', 'POST'])
@login_required
def about_me():
    form = RegisterForm()
    user = User.query.filter_by(login=form.login.data).first()
    if form.validate_on_submit():
        try:
            if not (form.login.data or form.password2.data or form.password1.data):
                flash('Все поля должны быть заполнены')
                return render_template('about-me.html', form=form)
            elif form.password1.data != form.password2.data:
                flash('Пароли не сходятся')
                return render_template('about-me.html', form=form)
            else:
                hash_pwd = generate_password_hash(form.password1.data)
                if form.deliver.data:
                    user.login=form.login.data
                    user.password=hash_pwd
                    user.email=form.email.data
                    user.deliver=form.deliver.data
                    user.admin=user.admin
                    user.fio=form.fio.data
                    user.number=form.number.data
                else:
                    user.login=form.login.data
                    user.password=hash_pwd
                    user.email=form.email.data
                    user.deliver='Нет данных'
                    user.admin=user.admin
                    user.fio = form.fio.data
                    user.number = form.number.data
                db.session.commit()
                return redirect(url_for('about_me', form=form))
        except:
            flash('Произошла ошибка')
            return render_template('about-me.html', form=form)
    else:
        form.email.data = flask_user.current_user.email
        form.login.data = flask_user.current_user.login
        form.deliver.data = flask_user.current_user.deliver
        form.fio.data = flask_user.current_user.fio
        form.number.data = flask_user.current_user.number
        return render_template('about-me.html', form=form)


@app.route('/users')
@login_required
def userInfo():
    if flask_user.current_user.admin:
        users = User.query.all()
        return render_template('users.html', users=users)
    else:
        return abort(404)


@app.route('/users/delete/<int:id>')
@login_required
def userDelete(id):
    if flask_user.current_user.admin:
        user = User.query.filter_by(id=id).first()
        db.session.delete(user)
        db.session.commit()
        return redirect('/users')
    else:
        return abort(404)


@manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)

