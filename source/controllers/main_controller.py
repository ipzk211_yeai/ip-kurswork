import os
import flask_user
from flask import Flask, render_template, url_for, request, redirect, flash
from flask_login import login_required
from werkzeug.utils import secure_filename
from cfg import app, manager, db
from models.product_item import ProductItem
from utils import allowed_file


@app.route('/<string:category>/<int:id>', methods=['GET', 'POST'])
def filter_index(category, id=0):
    if request.method == "POST":
        name = request.form['search']
        products = ProductItem.query.filter_by(category=category).filter(ProductItem.title.contains(name)).offset(id * 6).limit(6)
        count = ProductItem.query.filter_by(category=category).filter(ProductItem.title.contains(name)).count()
        return render_template("index.html", products=products, id=id, count=count/6)
    else:
        products = ProductItem.query.filter_by(category=category).offset(id * 6).limit(6)
        count = ProductItem.query.filter_by(category=category).count()
        return render_template("index.html", products=products, id=id, count=count/6)


@app.route('/')
def index():
    return redirect('/0')


@app.route('/<int:id>', methods=['GET', 'POST'])
def shop(id=0):
    if request.method == "POST":
        name = request.form['search']
        products = ProductItem.query.filter(ProductItem.title.contains(name)).offset(id * 6).limit(6)
        count = ProductItem.query.filter(ProductItem.title.contains(name)).count()
        return render_template("index.html", products=products, id=id, count=count/6)
    else:
        products = ProductItem.query.offset(id*6).limit(6)
        count = ProductItem.query.count()
        return render_template("index.html", products=products, id=id, count=count/6)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@app.route('/add', methods=['GET', 'POST'])
@login_required
def add():
    if flask_user.current_user.admin:
        if request.method == "POST":
            title = request.form['title']
            price = request.form['price']
            desc = request.form['track1']
            category = request.form['category']
            if 'image' not in request.files:
                flash('Не вижу файл')
                return render_template("add.html")
            file = request.files['image']

            if file.filename == '':
                flash('Не вижу файл')
                return render_template("add.html")
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            product = ProductItem(title=title, price=price, description=desc, pic=secure_filename(file.filename), category=category)
            try:
                db.session.add(product)
                db.session.commit()
                return redirect('/')
            except:
                flash('При добавлении произошла ошибка')
                return render_template("add.html")
        else:
            return render_template("add.html")
    else:
        return redirect('/0')


@app.route('/product/<int:id>')
def product_detail(id):
    product = ProductItem.query.get(id)
    return render_template("product-detail.html", product=product)


@app.route('/product/<int:id>/delete')
@login_required
def product_delete(id):
    if flask_user.current_user.admin:
        product = ProductItem.query.get_or_404(id)
        try:
            db.session.delete(product)
            db.session.commit()
            return redirect('/0')
        except:
            return "При удалении произошла ошибка"
    else:
        return redirect('/0')


@app.route('/product/<int:id>/update', methods=['GET', 'POST'])
@login_required
def product_update(id):
    if flask_user.current_user.admin:
        product = ProductItem.query.get(id)
        if request.method == "POST":
            product.title = request.form['title']
            product.price = request.form['price']
            product.description = request.form['track1']
            product.category = request.form['category']
            if 'image' not in request.files:
                flash('Не вижу файл')
                return render_template("product-update.html", product=product)
            file = request.files['image']
            if file.filename == '':
                file.filename = product.pic
            else:
                if file and allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            product.pic = secure_filename(file.filename)
            try:
                db.session.commit()
                return redirect('/0')
            except:
                flash('При добавлении произошла ошибка')
                return render_template("product-update.html", product=product)
        else:
            product = ProductItem.query.get(id)
            return render_template("product-update.html", product=product)
    else:
        return redirect('/0')
