from cfg import db


class Deliver(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    products = db.Column(db.String(100), nullable=False)
    count = db.Column(db.String(100), nullable=False)
    deliverTo = db.Column(db.String(100), nullable=False)
    userEmail = db.Column(db.String(64), nullable=False)
    userLogin = db.Column(db.String(40), nullable=False)
    userFIO = db.Column(db.String(60), nullable=False)
    userNumber = db.Column(db.String(10), nullable=False)

    def __repr__(self):
        return '<Deliver %r>' % self.id
