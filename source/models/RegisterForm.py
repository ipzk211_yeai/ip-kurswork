from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired, Email


class RegisterForm(FlaskForm):
    login = StringField('login', validators=[DataRequired()])
    password1 = PasswordField('password1', validators=[DataRequired()])
    password2 = PasswordField('password2', validators=[DataRequired()])
    email = StringField('email', validators=[DataRequired(), Email(message="Это не почта")])
    deliver = StringField('deliver')
    fio = StringField('fio')
    number = StringField('number')
