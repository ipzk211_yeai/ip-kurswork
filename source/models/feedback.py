from cfg import db


class Feedback(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    userMessage = db.Column(db.String(250), nullable=False)
    userEmail = db.Column(db.String(64), nullable=False)
    userLogin = db.Column(db.String(40), nullable=False)

    def __repr__(self):
        return '<Deliver %r>' % self.id
