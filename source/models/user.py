from flask_login import UserMixin
from cfg import db


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(24), nullable=False, unique=True)
    password = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(64), nullable=False)
    deliver = db.Column(db.String(100))
    admin = db.Column(db.Boolean)
    fio = db.Column(db.String(60), nullable=False)
    number = db.Column(db.String(10))

