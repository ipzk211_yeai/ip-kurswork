from cfg import db


class ProductItem(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    pic = db.Column(db.String(50), nullable=False)
    title = db.Column(db.String(50), nullable=False)
    description = db.Column(db.String(200), nullable=False)
    price = db.Column(db.Float, nullable=False)
    category = db.Column(db.String(50), nullable=False)

    def __repr__(self):
        return '<Product %r>' % self.id
